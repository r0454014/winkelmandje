<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 13:52
 */
class Discount
{
    private $productId;
    private $percentage;

    /**
     * Discount constructor.
     * @param $productId
     * @param $percentage
     */
    public function __construct($productId, $percentage)
    {
        $this->productId = $productId;
        $this->percentage = $percentage;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }




}