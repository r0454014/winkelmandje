<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 15:01
 */
class DiscountCalculator
{

    public static function calculateDiscount(Product $product, $discountArray) {
        $value = 0;
        foreach ($discountArray as $discount) {
            if ($discount->productId == $product->getProductId()) {
                $price = $product->getPrice();
                $discountPrice = $price * ($discount->percentage / 100);
                return $price - $discountPrice;
            }
        }
        return $product->getPrice();
    }
}