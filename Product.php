<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 13:24
 */
class Product
{

    private $productId;
    private $name;
    private $price;

    /**
     * Product constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }





}