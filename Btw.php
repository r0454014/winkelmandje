<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 13:24
 */
class Btw
{
    private $countryCode;
    private $percentage;

    /**
     * Btw constructor.
     * @param $countryCode
     * @param $value
     */
    public function __construct($countryCode, $value)
    {
        $this->countryCode = $countryCode;
        $this->percentage = $value;
    }


    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }


}