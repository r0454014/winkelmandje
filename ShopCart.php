<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 13:23
 */
class ShopCart
{

    private $products;
    private $btw;
    private $discounts;

    /**
     * ShopCart constructor.
     */
    public function __construct()
    {
        $this->products = array();
        $this->discount = array();
    }

    public function addProduct(Product $product) {
        $this->products[] = $product;
    }


    public function addDiscount($productId, $percentage) {
        $discountAdder = new DiscountAdder($this->discounts);
        $this->discounts = $discountAdder->addDiscount($productId, $percentage);
    }

    public function showBtw() {
        return BtwCalculator::calculate($this->btw, $this->products, $this->discounts);
    }

    public function assignBtw($countryCode, $value) {
        $this->btw = new Btw($countryCode, $value);
    }


}