<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 14:41
 */
class BtwCalculator
{

    public static function calculate(Btw $btw, $products, $discounts) {
        $totalPrice = 0;
        $btwPrice = 0;
        foreach ($products as $product) {
            $totalPrice += DiscountCalculator::calculateDiscount($product, $discounts);
        }
        $btwPrice = $totalPrice * ($btw->getPercentage() / 100);
        return $btwPrice;
    }
}