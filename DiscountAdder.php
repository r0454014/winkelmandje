<?php

/**
 * Created by PhpStorm.
 * User: Maikel Tielens
 * Date: 19/07/2016
 * Time: 14:12
 */
class DiscountAdder
{

    private $discountArray;

    /**
     * DiscountAdder constructor.
     * @param $discounts
     */
    public function __construct($discounts)
    {
        $this->discountArray = $discounts;
    }

    public function addDiscount($productId, $percentage) {
        $canAddDiscount = true;

        for ($i = 0; $i < count($this->discountArray); $i++) {
            $discount = $this->discountArray[$i];

            if ($discount->productId == $productId) {
                $this->discountArray[$i] = new Discount($productId, $percentage);
                $canAddDiscount = false;
                break;
            }
        }

        if ($canAddDiscount) {
            $this->discountArray[] = new Discount($productId, $percentage);
        }

        return $this->discountArray;
    }
}